package com.bh.relevadorWS.dao;


import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class CustomHibernateDao<T, ID extends Serializable> implements IGenericDAO<T,ID > {
	
	
	public Class<T> domainClass = getDomainClass();
	protected Class getDomainClass() {
		 
		if (domainClass == null) {
			ParameterizedType thisType = (ParameterizedType) getClass()
					.getGenericSuperclass();
			domainClass = (Class) thisType.getActualTypeArguments()[0];
		}
		return domainClass;
	}
	
	public ID save(T object) {
		Session session = null;
		ID id; 
		try {
			
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		}
		try {
			
			session.beginTransaction();
				id = (ID) session.save(object);
			session.getTransaction().commit();
		} catch(HibernateException he) {
			
			session.getTransaction().rollback();
			id = null;
			he.printStackTrace();
		}
		session.close();
		return id;
		
	}

	
	public T findById(ID id) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		
		 T object = (T) session.get(domainClass,id); 
		 session.close();
		 return object;
	}

	public List<T> findAll() {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		List<T> list = criteria.list();
		session.close();
		return list ;
	}

	public void delete(T object) {
		
		Session session = null;		 
		try {
			
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		}
		
		try {
			
			session.beginTransaction();
				session.delete(object);
			session.getTransaction().commit();
		}catch(HibernateException he) {
			session.getTransaction().rollback();
		}
		session.close();
	}

	public void update(T object) {
		
		Session session = null;
		try {
			
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		}
		
		try {
			
			session.beginTransaction();
				session.update(object);
			session.getTransaction().commit();
		}catch(HibernateException he) {
			session.getTransaction().rollback();
		}
		session.close();
	}

	public List<T> findByCriteria(T t) {
		// TODO Auto-generated method stub
		return null;
	}
	//Custom
	
}
