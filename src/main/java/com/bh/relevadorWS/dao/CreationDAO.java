package com.bh.relevadorWS.dao;

import com.bh.relevadorWS.model.Creation;

public class CreationDAO extends CustomHibernateDao<Creation, Integer> {
	
	//Singleton
	private static CreationDAO instance;

	public static CreationDAO getInstance(){
		
		if(instance == null){
			
			instance = new CreationDAO();
		}
		return instance;
	}
	
	public boolean isNotCreated(){
		
		Creation creation = findById(1);
		if(creation == null)
			return true;
		else
			return false;
	}
}
