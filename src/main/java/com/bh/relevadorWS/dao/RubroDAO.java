package com.bh.relevadorWS.dao;

import com.bh.relevadorWS.model.Rubro;

public class RubroDAO extends CustomHibernateDao<Rubro, Integer> {
	
	private static RubroDAO instance;
	public static RubroDAO getInstance(){
		
		if(instance == null){
			
			instance = new RubroDAO();
		}
		return instance;
	}
}
