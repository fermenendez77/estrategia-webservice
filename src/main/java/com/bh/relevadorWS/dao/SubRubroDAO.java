package com.bh.relevadorWS.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.bh.relevadorWS.model.SubRubro;

public class SubRubroDAO extends CustomHibernateDao<SubRubro, Integer> {
	
	private static SubRubroDAO instance;
	public static SubRubroDAO getInstance(){
		
		if(instance == null){
			
			instance = new SubRubroDAO();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public List<SubRubro> findNews(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaCreacion", date ));
		List<SubRubro> list = criteria.list();
		session.close();
		return list ;
	}
	
	@SuppressWarnings("unchecked")
	public List<SubRubro> findUpdates(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaModificacion", date ));
		List<SubRubro> list = criteria.list();
		session.close();
		return list ;
	}
}
