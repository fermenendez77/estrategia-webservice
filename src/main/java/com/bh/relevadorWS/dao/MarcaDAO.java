package com.bh.relevadorWS.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import com.bh.relevadorWS.model.Marca;


public class MarcaDAO extends CustomHibernateDao<Marca, Integer> {
	
	private static MarcaDAO instance;
	public static MarcaDAO getInstance(){
		
		if(instance == null){
			
			instance = new MarcaDAO();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public List<Marca> findNews(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaCreacion", date ));
		List<Marca> list = criteria.list();
		session.close();
		return list ;
	}
	
	@SuppressWarnings("unchecked")
	public List<Marca> findUpdates(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaModificacion", date ));
		List<Marca> list = criteria.list();
		session.close();
		return list ;
	}
}
