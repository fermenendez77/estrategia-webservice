package com.bh.relevadorWS.dao;

import com.bh.relevadorWS.model.Lugar;

public class LugarDAO extends CustomHibernateDao<Lugar,Integer > {
		
	private static LugarDAO instance;
	public static LugarDAO getInstance(){
		
		if(instance == null){
			
			instance = new LugarDAO();
		}
		return instance;
	}
}
