package com.bh.relevadorWS.dao;



import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.bh.relevadorWS.model.Calle;

public class CalleDAO extends CustomHibernateDao<Calle,Integer>  {

	private static CalleDAO instance;
	public static CalleDAO getInstance(){
		
		if(instance == null){
			
			instance = new CalleDAO();
		}
		return instance;
	}
		
	@SuppressWarnings("unchecked")
	public List<Calle> findNews(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaCreacion", date ));
		List<Calle> list = criteria.list();
		session.close();
		return list ;
	}
}
