package com.bh.relevadorWS.dao;


import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T, ID extends Serializable> {

	public ID save(T object);
	
	public T findById(ID id);
	
	public List<T> findAll();
	
	public void delete (T object);
	
	public void update (T object);
	
	public List<T> findByCriteria(T t);
	
}
