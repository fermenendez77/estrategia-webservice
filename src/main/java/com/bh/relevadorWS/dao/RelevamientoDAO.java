package com.bh.relevadorWS.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import com.bh.relevadorWS.model.Relevamiento;

public class RelevamientoDAO extends CustomHibernateDao<Relevamiento, Long> {
	
	private static RelevamientoDAO instance;
	public static RelevamientoDAO getInstance(){
		
		if(instance == null){
			
			instance = new RelevamientoDAO();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public List<Relevamiento> findNews(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaCreacion", date ));
		List<Relevamiento> list = criteria.list();
		session.close();
		return list ;
	}
	
	@SuppressWarnings("unchecked")
	public List<Relevamiento> findUpdates(Date date) {
		
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			session = HibernateUtil.getSessionFactory().openSession();
		} 
		Criteria criteria = session.createCriteria(domainClass); 	 
		criteria.add(Restrictions.ge("fechaModificacion", date ));
		List<Relevamiento> list = criteria.list();
		session.close();
		return list ;
	}
	
}
