package com.bh.relevadorWS.pob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bh.relevadorWS.dao.CalleDAO;
import com.bh.relevadorWS.dao.CreationDAO;
import com.bh.relevadorWS.dao.LugarDAO;
import com.bh.relevadorWS.dao.MarcaDAO;
import com.bh.relevadorWS.dao.RubroDAO;
import com.bh.relevadorWS.dao.SubRubroDAO;
import com.bh.relevadorWS.model.Calle;
import com.bh.relevadorWS.model.Creation;
import com.bh.relevadorWS.model.Lugar;
import com.bh.relevadorWS.model.Marca;
import com.bh.relevadorWS.model.Rubro;
import com.bh.relevadorWS.model.SubRubro;

public class PoblateListener implements ServletContextListener {
	
	
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
		CreationDAO creationDAO = CreationDAO.getInstance();
		if(creationDAO.isNotCreated()){
			insertCalles("calles.csv");
			insertMarcas("marcas.csv");
			insertLugares("lugares.csv");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date date = new Date();
			Creation creation = new Creation(date);
			creationDAO.save(creation);
		}
	}
	
public static void insertCalles(String filename){
		
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
		BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
		CalleDAO calleDAO = CalleDAO.getInstance();
		String line = ""; 
		Date date = new Date();
		int count = 0;
		try {
			while((line = buffer.readLine()) != null){
				
				   String[] columns = line.split(";");
				   Calle calle = new Calle();
				   calle.setNombre(columns[0]);
				   calle.setMaxNum(Integer.valueOf(columns[1]));
				   calle.setFechaCreacion(date);
				   calleDAO.save(calle);
				   count++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se insertaron :" +count +" calles");
	}
	
	public static void insertLugares(String filename){
		
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
		BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
		LugarDAO lugarDAO = LugarDAO.getInstance();
		String line = ""; 
		int count = 0;
		try {
			while((line = buffer.readLine()) != null){
				
				   String[] columns = line.split(";");
				   Lugar lugar = new Lugar();
				   lugar.setTipo(columns[0]);
				   lugarDAO.save(lugar);
				   count++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se insertaron :" +count +" lugares");
	}
	
	public static void insertMarcas(String filename){
		
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
		BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
		MarcaDAO marcaDAO = MarcaDAO.getInstance();
		SubRubroDAO subRubroDAO = SubRubroDAO.getInstance();
		RubroDAO rubroDAO = RubroDAO.getInstance();
		String line = ""; 
		int count = 0;
		Marca marca = new Marca();
		SubRubro subRubro = new SubRubro();
		Rubro rubro = new Rubro();
		try {
			while((line = buffer.readLine()) != null){
				
				   String[] columns = line.split(";");
				   if(!columns[0].equals("")){
					  
					   rubro = new Rubro();
					   rubro.setNombre(columns[0]);
					   rubroDAO.save(rubro);
				   }
				   if(!columns[1].equals("")){
					   Date date = new Date();
					   subRubro = new SubRubro();
					   subRubro.setNombre(columns[1]);
					   subRubro.setFechaCreacion(date);
					   subRubro.setFechaModificacion(date);
					   subRubro.setActivo(1);
					   subRubro.setRubro(rubro);
					   subRubroDAO.save(subRubro);
				   }
				   Date date = new Date();
				   marca = new Marca();
				   marca.setNombre(columns[2]);
				   marca.setFechaCreacion(date);
				   marca.setFechaModificacion(date);
				   marca.setActivo(1);
				   marca.setRubro(rubro);
				   marca.setSubRubro(subRubro);
				   marcaDAO.save(marca);
				   count++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se insertaron :" +count +" Marcas");
	}
}
