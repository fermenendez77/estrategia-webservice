package com.bh.relevadorWS.dto;

import java.util.ArrayList;
import java.util.List;

import com.bh.relevadorWS.model.Relevamiento;


public class RelevamientoDTO {
	
	private  List<Relevamiento> relevamientos;
	public RelevamientoDTO(){
		 relevamientos = new ArrayList<Relevamiento>();
	}
	public java.util.List<Relevamiento> getRelevamiento() {
		return relevamientos;
	}

	public void setRelevamiento(List<Relevamiento> relevamientos) {
		this.relevamientos = relevamientos;
	}
	
}
