package com.bh.relevadorWS.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties({"t"})
public abstract class AllDto<T> {
	
	
	@JsonProperty(value="objects")
	private List<T> objects;

	public List<T> getT() {
		return objects;
	}

	public void setT(List<T> t) {
		objects = t;
	}
	
}
