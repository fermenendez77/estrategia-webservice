package com.bh.relevadorWS.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bh.relevadorWS.dao.CreationDAO;
import com.bh.relevadorWS.dao.SubRubroDAO;
import com.bh.relevadorWS.dto.AllDto;
import com.bh.relevadorWS.model.SubRubro;
@Path("/api/subrubro")
public class SubRubroService {
	
	private SubRubroDAO subRubroDAO;
	private CreationDAO creationDAO; 
	private Date creationDate;
	
	public SubRubroService(){
		
		this.subRubroDAO = SubRubroDAO.getInstance();
		this.creationDAO = CreationDAO.getInstance(); 
		this.creationDate = creationDAO.findById(1).getCreationDate();
	}
	@POST
	@Path("/put")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putSubRubro(SubRubro subRubro, @Context HttpServletRequest request){
		
		Date date = new Date();
		subRubro.setFechaCreacion(date);
		subRubro.setFechaModificacion(creationDate);
		subRubro.setActivo(1);
		subRubroDAO.save(subRubro);
		return Response.status(200).entity(subRubro).build();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSubRubro(SubRubro subRubro, @Context HttpServletRequest request){
		
		Date date = new Date();
		SubRubro persist = subRubroDAO.findById(subRubro.getId());
		subRubro.setFechaCreacion(persist.getFechaCreacion());
		subRubro.setFechaModificacion(date);
		subRubro.setActivo(1);
		subRubroDAO.update(subRubro);
		return Response.status(200).entity(subRubro).build();
	}
	
	@GET
	@Path("/getNews")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNews(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(subRubroDAO.findNews(date));
		return Response.status(200).entity(dto).build();
	}
	@GET
	@Path("/getUpdates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdates(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(subRubroDAO.findUpdates(date));
		return Response.status(200).entity(dto).build();
	}
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@QueryParam("id") Integer id,@Context HttpServletRequest request){
		
		SubRubro subrubro = subRubroDAO.findById(id);
		return Response.status(200).entity(subrubro).build();
	}
	
	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@Context HttpServletRequest request){
		
		Dto dto = new Dto();
		dto.setT(subRubroDAO.findAll());
		return Response.status(200).entity(dto).build();
	}
	
	

	class Dto extends AllDto<SubRubro>{}
}
