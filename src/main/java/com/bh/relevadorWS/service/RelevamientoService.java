package com.bh.relevadorWS.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bh.relevadorWS.dao.CreationDAO;
import com.bh.relevadorWS.dao.RelevamientoDAO;
import com.bh.relevadorWS.dto.AllDto;
import com.bh.relevadorWS.dto.ResponseRelevamientoDTO;
import com.bh.relevadorWS.model.Relevamiento;

@Path("/api/relevamiento")
public class RelevamientoService {
	
	private Date creationDate;
	private RelevamientoDAO relevamientoDAO;
	private CreationDAO creationDAO;
	public RelevamientoService(){
		
		this.relevamientoDAO = RelevamientoDAO.getInstance();
		this.creationDAO = CreationDAO.getInstance();
		this.creationDate = creationDAO.findById(1).getCreationDate();
	} 
	@POST
	@Path("/put")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putRelevamiento(Relevamiento relevamiento, @Context HttpServletRequest request){
		
		Date date = new Date();
		relevamiento.setFechaCreacion(date);
		relevamiento.setFechaModificacion(creationDate);
		Long id = relevamiento.getId();
		if(relevamiento.getMarca()!=null)
			relevamiento.getMarca().getSubRubro().setRubro(relevamiento.getMarca().getRubro());
		Long r = relevamientoDAO.save(relevamiento);
		ResponseRelevamientoDTO dto;
		int code;
		if(r!=null){
			
			dto = new ResponseRelevamientoDTO("OK", relevamiento.getId() , id);
			code = 200;
		}else{
			
			dto = new ResponseRelevamientoDTO("ERROR", 0, id);
			code = 401;
		}
		
		return Response.status(code).entity(dto).build();
	}
	
	@GET
	@Path("/getNews")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNews(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(relevamientoDAO.findNews(date));
		return Response.status(200).entity(dto).build();
	}
	
	@GET
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@QueryParam("id") long id,@Context HttpServletRequest request){
		
		Relevamiento relevamiento = relevamientoDAO.findById(id);
		relevamientoDAO.delete(relevamiento);
		return Response.status(200).entity("OK").build();
	}
	
	@GET
	@Path("/getUpdates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdates(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(relevamientoDAO.findUpdates(date));
		return Response.status(200).entity(dto).build();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateRelevamiento(Relevamiento relevamiento, @Context HttpServletRequest request){
		
		Date date = new Date();
		Relevamiento rel = relevamientoDAO.findById(relevamiento.getId());
		relevamiento.setFechaModificacion(date);
		relevamiento.setFechaCreacion(rel.getFechaCreacion());
		Long id = relevamiento.getId();
		if(relevamiento.getMarca()!=null)
			relevamiento.getMarca().getSubRubro().setRubro(relevamiento.getMarca().getRubro());
		relevamientoDAO.update(relevamiento);
		ResponseRelevamientoDTO dto;		
		dto = new ResponseRelevamientoDTO("OK", relevamiento.getId() , id);
		int code = 200;	
		return Response.status(code).entity(dto).build();
	}
	class Dto extends AllDto<Relevamiento>{}
	
}
