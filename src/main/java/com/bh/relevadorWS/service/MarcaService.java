package com.bh.relevadorWS.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bh.relevadorWS.dao.CreationDAO;
import com.bh.relevadorWS.dao.MarcaDAO;
import com.bh.relevadorWS.dto.AllDto;
import com.bh.relevadorWS.model.Marca;


@Path("/api/marca")
public class MarcaService {
	
	private MarcaDAO marcaDAO; 
	private CreationDAO creationDAO;
	private Date creationDate;
	public MarcaService(){
		
		this.marcaDAO = MarcaDAO.getInstance();
		this.creationDAO = CreationDAO.getInstance();
		this.creationDate = creationDAO.findById(1).getCreationDate();
	}
	
	@POST
	@Path("/put")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putMarca(Marca marca, @Context HttpServletRequest request){
		
		
		Date date = new Date();
		marca.getSubRubro().setActivo(1);
		marca.getSubRubro().setRubro(marca.getRubro());
		marca.setFechaCreacion(date);
		marca.setFechaModificacion(creationDate);
		marca.setActivo(1);
		marcaDAO.save(marca);
		return Response.status(200).entity(marca).build();
	}
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateMarca(Marca marca, @Context HttpServletRequest request){
				
		Date date = new Date();
		Marca persist = marcaDAO.findById(marca.getId());
		marca.setFechaCreacion(persist.getFechaCreacion());
		marca.getSubRubro().setActivo(1);
		marca.getSubRubro().setRubro(marca.getRubro());
		marca.setFechaModificacion(date);
		marca.setActivo(1);
		marcaDAO.update(marca);
		return Response.status(200).entity(marca).build();
	}
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@QueryParam("id") Integer id, @Context HttpServletRequest request){
		
		Marca marca = marcaDAO.findById(id);
		return Response.status(200).entity(marca).build();
	}
	
	@GET
	@Path("/getNews")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNews(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(marcaDAO.findNews(date));
		return Response.status(200).entity(dto).build();
	}
	@GET
	@Path("/getUpdates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUpdates(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(marcaDAO.findUpdates(date));
		return Response.status(200).entity(dto).build();
	}
	
	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@Context HttpServletRequest request){
		
		Dto dto = new Dto();
		dto.setT(marcaDAO.findAll());
		return Response.status(200).entity(dto).build();
	}
	
	
	class Dto extends AllDto<Marca>{}
}
