package com.bh.relevadorWS.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bh.relevadorWS.dao.CalleDAO;
import com.bh.relevadorWS.dto.AllDto;
import com.bh.relevadorWS.model.Calle;

@Path("/api/calle")
public class CalleService {
	
	CalleDAO calleDAO = CalleDAO.getInstance();
	@POST
	@Path("/put")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putCalle(Calle calle, @Context HttpServletRequest request){
		
		Date date = new Date();
		calle.setFechaCreacion(date);
		calleDAO.save(calle);
		return Response.status(200).entity(calle).build();
	}
	
	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@Context HttpServletRequest request){
		
		Dto dto = new Dto();
		dto.setT(calleDAO.findAll());
		return Response.status(200).entity(dto).build();
	}
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@QueryParam("id") Integer id, @Context HttpServletRequest request){
		
		//Calle calle = calleDAO.findById(id);
		Calle calle = calleDAO.findById(id);
		return Response.status(200).entity(calle).build();
	}
	@GET
	@Path("/getNews")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNews(@QueryParam("date") String stringdate,@Context HttpServletRequest request){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = format.parse(stringdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dto dto = new Dto();
		dto.setT(calleDAO.findNews(date));
		return Response.status(200).entity(dto).build();
	}

	class Dto extends AllDto<Calle>{}
}
