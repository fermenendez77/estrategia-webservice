package com.bh.relevadorWS.service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bh.relevadorWS.dao.RubroDAO;
import com.bh.relevadorWS.dto.AllDto;
import com.bh.relevadorWS.model.Rubro;
@Path("/api/rubro")
public class RubroService {
	
	RubroDAO rubroDAO = RubroDAO.getInstance();
	
	@POST
	@Path("/put")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putRubro(Rubro rubro, @Context HttpServletRequest request){
		
		rubroDAO.save(rubro);	
		return Response.status(200).entity(rubro).build();
	}
	
	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@Context HttpServletRequest request){
		
		Dto dto = new Dto();
		dto.setT(rubroDAO.findAll());
		return Response.status(200).entity(dto).build();
	}
	
	class Dto extends AllDto<Rubro>{}
}
