package com.bh.relevadorWS.service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.bh.relevadorWS.dao.CreationDAO;
import com.bh.relevadorWS.model.Creation;

@Path("/api/creation")
public class CreationService {
	
	CreationDAO creationDAO = CreationDAO.getInstance();
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context HttpServletRequest request){
		
		Creation creation = creationDAO.findById(1);
		return Response.status(200).entity(creation).build();
	}
}
