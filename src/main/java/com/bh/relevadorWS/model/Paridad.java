package com.bh.relevadorWS.model;

/**
 * Created by fermenendez on 30/8/15.
 */
public enum Paridad {

    PAR,
    IMPAR
}
