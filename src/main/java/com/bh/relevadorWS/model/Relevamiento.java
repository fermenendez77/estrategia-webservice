package com.bh.relevadorWS.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by fermenendez on 30/8/15.
 */
@JsonIgnoreProperties({"sync","remoteId","activo","orientacion"})
@Entity
@Table(name = "RELEVAMIENTO")
public class Relevamiento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
    private Long id;
    
	@ManyToOne
	@JoinColumn(name = "ID_CALLE")
	private Calle calle;
    
	@ManyToOne
	@JoinColumn(name = "ID_LUGAR")
	private Lugar lugar;
    
	@Column(name = "NUMERACION")
    private Integer numeracion;
    
	@Column(name = "POSICION")
    private Integer posicion;
    
	@Column(name = "PARIDAD")
    private String paridad;
	
	@ManyToOne
	@JoinColumn(name = "ID_MARCA")
    private Marca marca;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT-3")
    @Column(name = "FECHA")
    private Date fecha;
    
    @Column(name = "DATE_MODIFY")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT-3")
	private Date fechaModificacion;
    
    @Column(name = "DATE_CREATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT-3")
	private Date fechaCreacion;
    
    public Calle getCalle() {
        return calle;
    }

    public void setCalle(Calle calle) {
        this.calle = calle;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public Integer getNumeracion() {
        return numeracion;
    }

    public void setNumeración(Integer numeracion) {
        this.numeracion = numeracion;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getParidad() {
        return paridad;
    }

    public void setParidad(String paridad) {
        this.paridad = paridad;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
}
