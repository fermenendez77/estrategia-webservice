package com.bh.relevadorWS.model;

/**
 * Created by fermenendez on 29/8/15.
 */
public enum Tipo {


    CASA(1),
    LOTE(2),
    LOCAL_VACIO(3),
    LOCAL_OCUPADO(4),
    EDIFICIO_RESIDENCIAL(5),
    EDIFICIO_RESIDENCIAL_CONSTRUCCION(6),
    EDIFICIO_COMERCIAL(7),
    EDIFICIO_COMERCIAL_CONSTRUCCION(8);
    private int numVal;

    Tipo(int numval){

        this.numVal = numval;
    }

    public int getNumVal(){

        return numVal;
    }
}
